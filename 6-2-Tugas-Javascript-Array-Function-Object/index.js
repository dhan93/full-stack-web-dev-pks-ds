// soal 1
console.log("### soal 1 ###");

const daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

let hewanUrut = daftarHewan.sort()

hewanUrut.forEach(el => {
  console.log(el);
});
console.log("");



// soal 2
console.log("### soal 2 ###");

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" };

function introduce(data) {
  return "Nama saya "+data.name+", umur saya "+data.age+" tahun, alamat saya di "+data.address+", dan saya punya hobby yaitu "+data.hobby
}

var perkenalan = introduce(data)

console.log(perkenalan)
console.log("");



// soal 3
console.log("### soal 3 ###");
function cek_vokal(huruf) {
  const vokal = ['a', 'i', 'u', 'e', 'o'];
  return vokal.includes(huruf.toLowerCase())
}

function hitung_huruf_vokal(kata) {
  let totalVokal = 0;
  Array.from(kata).forEach(el => {
    if (cek_vokal(el)) {
      totalVokal += 1
    }
  });
  return totalVokal;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)
console.log("");



// soal 4
console.log("### soal 4 ###");
function hitung(int) {
  return int + (int - 2)
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8