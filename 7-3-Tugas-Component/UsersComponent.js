export const UserList = {
  template: `
    <div class="usersList">
      <ul>
          <li v-for="(user , index) in users" :key="index">
              {{ user.name }} ||
              <button @click="editUser(index)">Edit</button>
              <button @click="deleteUser(index)">Delete</button>
          </li>
      </ul>
    </div>
  `,
  props: ['users'],
  methods: {
    editUser(index) {
      this.$emit('editin-dong', index)
    },

    deleteUser(index) {
      this.$emit('deletin-dong', index)
    }
  }
}