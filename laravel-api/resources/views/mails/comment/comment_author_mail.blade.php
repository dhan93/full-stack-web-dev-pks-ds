<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  Hay <strong>{{$comment->user->name}}</strong>, komentar kamu di post <strong>{{$comment->post->title}}</strong> oleh <strong>{{$comment->post->user->name}}</strong> sudah berhasil disubmit.
  Isi komentarmu adalah: <strong>{{$comment->content}}</strong>
</body>
</html>