<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\OtpCode;
use Illuminate\Support\Carbon;
use App\Events\OtpRecreatedEvent;

class RegenerateOtpcodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
      //set validation
      $validator = Validator::make($request->all(), [
        'email'   => 'required|email',
      ]);
      
      //response error validation
      if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
      }

      $user = User::where('email', $request->email)->first();

      // remove previous otp code
      if ($user->otp_code) {
        $user->otp_code->delete();
      }
      

      // generate new unique otp code
      do {
        $random = mt_rand(100000, 999999);

        $check = OtpCode::where('otp', $random)->first();
      } while ($check);

      $now = Carbon::now();

      // store otp code
      $otp_code = OtpCode::create([
        'otp' => $random,
        'user_id' => $user->id,
        'valid_until' => $now->addMinutes(5)
      ]);

      event(new OtpRecreatedEvent($otp_code));

      return response()->json([
        'success' => true,
        'message' => 'OTP code regenerated',
        'data' => [
          'user' => $user,
          'otp_code' => $otp_code
        ]
      ]);
    }
}
