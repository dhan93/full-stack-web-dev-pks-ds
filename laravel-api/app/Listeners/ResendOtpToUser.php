<?php

namespace App\Listeners;

use App\Events\OtpRecreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\OtpRegeneratedMail;
use Illuminate\Support\Facades\Mail;

class ResendOtpToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpRecreatedEvent  $event
     * @return void
     */
    public function handle(OtpRecreatedEvent $event)
    {
      Mail::to($event->otpCode->user->email)->send(new OtpRegeneratedMail($event->otpCode));
    }
}
