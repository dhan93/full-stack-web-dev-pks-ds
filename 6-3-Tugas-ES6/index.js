// jawaban soal 1
const luasPersegi = (p, l) => {
  return p*l
}

const kelilingPersegi = (p, l) => {
  return 2*(p+l)
}

console.log(`luas persegi = ${luasPersegi(10,20)}`)
console.log(`keliling persegi = ${kelilingPersegi(10,20)}`)



// jawaban soal 2
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(`${firstName} ${lastName}`)
    }
  }
}

newFunction("William", "Imoh").fullName()


// jawaban soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject;

console.log(firstName, lastName, address, hobby)


// jawaban soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined)


// jawaban soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`

console.log(`before:\n ${before}`);
console.log(`after:\n ${after}`);