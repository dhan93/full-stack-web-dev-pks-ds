<?php 
class Hewan {
  protected $nama;

  public function __construct($nama)
  {
    $this->nama = $nama;
  }
}

class Komodo extends Hewan{
  public function getNama()
  {
    return $this->nama;
  }
}

$komodo = new Komodo('komo');
echo $komodo->getNama();


?>