<?php

  trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
      echo '<br/>';
      echo $this->nama;
      echo ' sedang ';
      echo $this->keahlian;
    }
  };

  trait Fight {
    public $attackPower;
    public $deffencePower;

    public function serang($target)
    {
      echo '<br/>';
      echo $this->nama.' sedang menyerang '.$target->nama;
      $this->diserang($target);
    }

    public function diserang($target)
    {
      echo '<br/>';
      echo $target->nama.' sedang diserang';
      $target->darah = $target->darah - ($this->attackPower/$target->deffencePower);
      
    }
  };

  class Elang {
    use Hewan, Fight;

    public function __construct($nama)
    {
      $this->nama = $nama;
      $this->jumlahKaki = 2;
      $this->keahlian = "terbang tinggi";
      $this->attackPower = 10;
      $this->deffencePower = 5;
    }

    public function getInfoHewan()
    {
      echo '<br/>Nama Hewan : '.$this->nama;
      echo '<br/>Jenis Hewan: Elang';
      echo '<br/>Darah : '.$this->darah;
      echo '<br/>Jumlah Kaki : '.$this->jumlahKaki;
      echo '<br/>Keahlian : '.$this->keahlian;
      echo '<br/>Attack power : '.$this->attackPower;
      echo '<br/>Deffence power : '.$this->deffencePower;
    }
  };

  class Harimau {
    use Hewan, Fight;

    public function __construct($nama)
    {
      $this->nama = $nama;
      $this->jumlahKaki = 4;
      $this->keahlian = "lari cepat";
      $this->attackPower = 7;
      $this->deffencePower = 8;
    }

    public function getInfoHewan()
    {
      echo '<br/>Nama Hewan : '.$this->nama;
      echo '<br/>Jenis Hewan: Harimau';
      echo '<br/>Darah : '.$this->darah;
      echo '<br/>Jumlah Kaki : '.$this->jumlahKaki;
      echo '<br/>Keahlian : '.$this->keahlian;
      echo '<br/>Attack power : '.$this->attackPower;
      echo '<br/>Deffence power : '.$this->deffencePower;
    }
  };

  $Elang = new Elang("si Elang");
  $Harimau = new Harimau("si Maung");

  echo $Elang->getInfoHewan();
  echo '<br/>';
  echo $Harimau->getInfoHewan();
  echo '<br/>';
  echo $Elang->atraksi();
  echo '<br/>';
  echo $Elang->serang($Harimau);
  echo '<br/>';
  echo $Elang->getInfoHewan();
  echo '<br/>';
  echo $Harimau->getInfoHewan();